#!/usr/bin/env python 

from urllib.request import urlopen
import json
docker_tags = []
page = 1
contains_tags = True

while contains_tags == True:
    try:
        docker_tags_url = "https://hub.docker.com/v2/repositories/mjasion/aws-cli/tags/?page_size=100&page=%d" % page
        with urlopen(docker_tags_url) as docker_tags_response:
            if docker_tags_response.status == 404:
                contains_tags = False
                break
            docker_tags_json = json.loads(docker_tags_response.read())
            docker_tags = docker_tags + [dt['name'] for dt in docker_tags_json['results']]
            page += 1
            if len(docker_tags_json['results']) < 100:
                contains_tags = False
    except Exception as e:
        contains_tags = False

github_tags_url = "https://api.github.com/repos/aws/aws-cli/tags?per_page=10"
github_tags_response = urlopen(github_tags_url)
github_tags_json = json.loads(github_tags_response.read())
aws_cli_tags = [gt['name'] for gt in github_tags_json]

tags_to_build = [at for at in aws_cli_tags if at not in docker_tags]
for tag in tags_to_build:
    print(tag)
