#!/usr/bin/env bash
if [[ ! -s awscli_versions ]]; then
    echo "Nothing to build"
    exit 0
fi

for awscli_version in `sort awscli_versions`; do
    printf "\nBuilding awscli($awscli_version) image on Debian with Python2\n\n"
    docker build --build-arg AWSCLI_VERSION=$awscli_version -t mjasion/aws-cli:$awscli_version-debian-python2 debian-python2

    printf "\nBuilding awscli($awscli_version) image on Debian with Python3\n\n"
    docker build --build-arg AWSCLI_VERSION=$awscli_version -t mjasion/aws-cli:$awscli_version-debian-python3 debian-python3
    docker tag mjasion/aws-cli:$awscli_version-debian-python3 mjasion/aws-cli:$awscli_version-debian

    printf "\nBuilding awscli($awscli_version) image on Alpine with Python2\n\n"
    docker build --build-arg AWSCLI_VERSION=$awscli_version -t mjasion/aws-cli:$awscli_version-alpine-python2 alpine-python2

    printf "\nBuilding awscli($awscli_version) image on Alpine with Python3\n\n"
    docker build --build-arg AWSCLI_VERSION=$awscli_version -t mjasion/aws-cli:$awscli_version-alpine-python3 alpine-python3
    docker tag mjasion/aws-cli:$awscli_version-alpine-python3 mjasion/aws-cli:$awscli_version-alpine

    docker tag mjasion/aws-cli:$awscli_version-debian-python3 mjasion/aws-cli:$awscli_version
done

AWSCLI_LATEST=`head -n1 awscli_versions`

printf "\n\nLatest awscli: $AWSCLI_LATEST\n\n"

docker tag mjasion/aws-cli:$AWSCLI_LATEST-alpine-python3 mjasion/aws-cli:alpine-python3
docker tag mjasion/aws-cli:$AWSCLI_LATEST-alpine-python3 mjasion/aws-cli:alpine

docker tag mjasion/aws-cli:$AWSCLI_LATEST-debian-python3 mjasion/aws-cli:debian-python3
docker tag mjasion/aws-cli:$AWSCLI_LATEST-debian-python3 mjasion/aws-cli:debian

docker tag mjasion/aws-cli:$AWSCLI_LATEST-debian-python3 mjasion/aws-cli:latest

docker push mjasion/aws-cli

docker images